//
//  ViewController.swift
//  MPC Manager
//
//  Created by Dan Burt on 30/07/2019.
//  Copyright © 2019 Redswitch Tech Ltd. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MPCManagerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var isAdvertising : Bool!
    
    @IBOutlet weak var tableViewPeers: UITableView!
    
    // MARK: ViewDidLoad Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.mpcManager.delegate = self
        appDelegate.mpcManager.browser.startBrowsingForPeers()
        isAdvertising = true
    }

    // MARK: Custom App Functions
    func stopStartAdvertising() {
        let actionSheet = UIAlertController(title: "", message: "Change Visibility", preferredStyle: UIAlertController.Style.actionSheet)
        
        var actionTitle : String
        if isAdvertising {
            actionTitle = "Make me invisible to others"
        } else {
            actionTitle = "Make me visible to others"
        }
        
        let visibilityAction = UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default) { (alertAction) in
            if self.isAdvertising {
                self.appDelegate.mpcManager.advertiser.stopAdvertisingPeer()
            } else {
                self.appDelegate.mpcManager.advertiser.startAdvertisingPeer()
            }
            self.isAdvertising = !self.isAdvertising
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (alertAction) in
            
        }
        
        actionSheet.addAction(visibilityAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.mpcManager.foundPeers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = appDelegate.mpcManager.foundPeers[indexPath.row].displayName
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPeer = appDelegate.mpcManager.foundPeers[indexPath.row] as MCPeerID
        
        appDelegate.mpcManager.browser.invitePeer(selectedPeer, to: appDelegate.mpcManager.session, withContext: nil, timeout: 20)
    }
    
    // MARK: MPCManager Delegate Methods
    func foundPeer() {
        tableViewPeers.reloadData()
    }
    
    func lostPeer() {
        tableViewPeers.reloadData()
    }
    
    func invitationWasRecieved(fromPeer: String) {
        let alert = UIAlertController(title: "", message: "\(fromPeer) wants to chat", preferredStyle: UIAlertController.Style.alert)
        
        let acceptAction : UIAlertAction(title: "Accept", style: UIAlertAction.Style.default) { (alertAction) in
            self.appDelegate.mpcManager.invitationHandler(true, self.appDelegate.mpcManager.session)
        }
        
        let declineAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (alertAction) in
            self.appDelegate.mpcManager.invitationHandler(false, nil)
        }
        
        alert.addAction(acceptAction)
        alert.addAction(declineAction)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func connectedWith(peer: MCPeerID) {
        <#code#>
    }

}

